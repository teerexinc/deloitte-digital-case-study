package com.deloitte.awaydayscheduler;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static com.deloitte.awaydayscheduler.Activity.*;

public class ActivityTests {

    @Test
    public void isActivity_returnsTrueWhenInputIsValid() {
        assertTrue(isActivity("Duck Herding 60min"));
        assertTrue(isActivity("Archery 45min"));
        assertTrue(isActivity("Salsa & Pickles sprint"));
        assertTrue(isActivity("2-wheeled Segways 45min"));
        assertTrue(isActivity("2020 60min"));
        assertTrue(isActivity("Wine sprint"));
        assertTrue(isActivity("Activity 0min"));
        assertTrue(isActivity("Activity 10MIN"));
        assertTrue(isActivity("Activity SPRINT"));
    }

    @Test
    public void isActivity_returnsFalseWhenInputIsInvalid() {
        assertFalse(isActivity("60min"));
        assertFalse(isActivity("sprint"));
        assertFalse(isActivity("Activity 30min detail"));
        assertFalse(isActivity("Activity min"));
        assertFalse(isActivity("Activity 1sprint"));
        assertFalse(isActivity("Activity print"));
    }

    @Test
    public void getDurationString_returnsCorrectResult() {
        assertEquals("SPRINT", new Activity("Activity SPRINT").getDurationString());
        assertEquals("0min", new Activity("Activity 0min").getDurationString());
    }

    @Test
    public void computeDuration_returnsCorrectResult() {
        assertEquals(15, new Activity("Wine sprint").computeDuration());
        assertEquals(1, new Activity("Activity 1min").computeDuration());
        assertEquals(9, new Activity("Activity 09min").computeDuration());
        assertEquals(66, new Activity("Activity 66min").computeDuration());
    }

}
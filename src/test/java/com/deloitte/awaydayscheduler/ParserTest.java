package com.deloitte.awaydayscheduler;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParserTest {

    @Test
    void parse() throws IOException {
        try (InputStream activitiesInputStream = ClassLoader.getSystemClassLoader().getResourceAsStream("activities.txt")) {
            List<Activity> activities = Parser.parse(activitiesInputStream);
            assertEquals(20, activities.size());
        }
    }
}
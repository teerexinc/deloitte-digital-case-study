package com.deloitte.awaydayscheduler;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SchedulerTests {

    @Test
    public void schedule_returnCorrectResult() {

        List<Activity> activities = new ArrayList<>();
        activities.add(new Activity("Duck Herding 60min"));
        activities.add(new Activity("Archery 45min"));
        activities.add(new Activity("Learning Magic Tricks 40min"));
        activities.add(new Activity("Laser Clay Shooting 60min"));
        activities.add(new Activity("Human Table Football 30min"));
        activities.add(new Activity("Buggy Driving 30min"));
        activities.add(new Activity("Salsa & Pickles sprint"));
        activities.add(new Activity("2-wheeled Segways 45min"));
        activities.add(new Activity("Viking Axe Throwing 60min"));
        activities.add(new Activity("Giant Puzzle Dinosaurs 30min"));
        activities.add(new Activity("Giant Digital Graffiti 60min"));
        activities.add(new Activity("Cricket 2020 60min"));
        activities.add(new Activity("Wine Tasting sprint"));
        activities.add(new Activity("Arduino Bonanza 30min"));
        activities.add(new Activity("Digital Tresure Hunt 60min"));
        activities.add(new Activity("Enigma Challenge 45min"));
        activities.add(new Activity("Monti Carlo or Bust 60min"));
        activities.add(new Activity("New Zealand Haka 30min"));
        activities.add(new Activity("Time Tracker sprint"));
        activities.add(new Activity("Indiano Drizzle 45min"));

        List<Team> teams = Scheduler.schedule(activities);

        String expectedResult = new StringBuilder()
                .append("Team 1:\n")
                .append("09:00 am : Duck Herding 60min\n")
                .append("10:00 am : Archery 45min\n")
                .append("10:45 am : Learning Magic Tricks 40min\n")
                .append("11:25 am : Human Table Football 30min\n")
                .append("11:55 am : Lunch Break 65min\n")
                .append("01:00 pm : Laser Clay Shooting 60min\n")
                .append("02:00 pm : Buggy Driving 30min\n")
                .append("02:30 pm : Salsa & Pickles sprint\n")
                .append("02:45 pm : 2-wheeled Segways 45min\n")
                .append("03:30 pm : Viking Axe Throwing 60min\n")
                .append("04:30 pm : Giant Puzzle Dinosaurs 30min\n")
                .append("05:00 pm : Staff Motivation Presentation\n")
                .append("Team 2:\n")
                .append("09:00 am : Giant Digital Graffiti 60min\n")
                .append("10:00 am : Cricket 2020 60min\n")
                .append("11:00 am : Wine Tasting sprint\n")
                .append("11:15 am : Arduino Bonanza 30min\n")
                .append("11:45 am : Time Tracker sprint\n")
                .append("12:00 pm : Lunch Break 60min\n")
                .append("01:00 pm : Digital Tresure Hunt 60min\n")
                .append("02:00 pm : Enigma Challenge 45min\n")
                .append("02:45 pm : Monti Carlo or Bust 60min\n")
                .append("03:45 pm : New Zealand Haka 30min\n")
                .append("04:15 pm : Indiano Drizzle 45min\n")
                .append("05:00 pm : Staff Motivation Presentation\n").toString();

        String actualResult = Scheduler.stringify(teams);

        assertEquals(expectedResult, actualResult);
    }
}
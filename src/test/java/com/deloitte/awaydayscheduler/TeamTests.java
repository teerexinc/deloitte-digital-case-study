package com.deloitte.awaydayscheduler;

import org.junit.jupiter.api.Test;

import static com.deloitte.awaydayscheduler.Team.getStartTime;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TeamTests {

    @Test
    public void getStartTime_returnsCorrectResult() {
        assertEquals("09:00 am", getStartTime(0));
        assertEquals("09:15 am", getStartTime(15));
        assertEquals("10:00 am", getStartTime(60));
        assertEquals("10:55 am", getStartTime(115));
        assertEquals("12:55 pm", getStartTime(235));
    }

    @Test
    public void addActivity_returnsCorrectResult() {
        Team team = new Team();
        assertEquals(240,
                team.addActivity(new Activity("Activity 60min")).getMaximumActivityDuration());
        assertEquals(120,
                team.addActivity(new Activity("Activity 200min")).getMaximumActivityDuration());
        assertEquals(40,
                team.addActivity(new Activity("Activity 100min")).getMaximumActivityDuration());
        assertEquals(20,
                team.addActivity(new Activity("Activity 40min")).getMaximumActivityDuration());
        assertEquals(0,
                team.addActivity(new Activity("Activity 20min")).getMaximumActivityDuration());
    }

    @Test
    public void addActivity_throwsIllegalArgumentException() {
        Team team = new Team();
        assertThrows(IllegalArgumentException.class,
                () -> team.addActivity(new Activity("Duck Herding 260min")));
    }

    @Test
    public void toString_returnsCorrectResult_team1() {
        String expected = new StringBuilder()
                .append("09:00 am : Duck Herding 60min\n")
                .append("10:00 am : Archery 45min\n")
                .append("10:45 am : Learning Magic Tricks 40min\n")
                .append("11:25 am : Human Table Football 30min\n")
                .append("01:00 pm : Laser Clay Shooting 50min\n")
                .append("01:50 pm : Buggy Driving 30min\n")
                .append("02:20 pm : Salsa & Pickles sprint\n")
                .append("02:35 pm : 2-wheeled Segways 45min\n")
                .append("03:20 pm : Viking Axe Throwing 60min\n")
                .append("04:20 pm : Giant Puzzle Dinosaurs 30min\n").toString();

        Team team = new Team();
        team.addActivity(new Activity("Duck Herding 60min"));
        team.addActivity(new Activity("Archery 45min"));
        team.addActivity(new Activity("Learning Magic Tricks 40min"));
        team.addActivity(new Activity("Laser Clay Shooting 50min"));
        team.addActivity(new Activity("Human Table Football 30min"));
        team.addActivity(new Activity("Buggy Driving 30min"));
        team.addActivity(new Activity("Salsa & Pickles sprint"));
        team.addActivity(new Activity("2-wheeled Segways 45min"));
        team.addActivity(new Activity("Viking Axe Throwing 60min"));
        team.addActivity(new Activity("Giant Puzzle Dinosaurs 30min"));
        assertEquals(expected, team.toString());
    }

    @Test
    public void toString_returnsCorrectResult_team2() {
        String expected = new StringBuilder()
                .append("09:00 am : Giant Digital Graffiti 45min\n")
                .append("09:45 am : Cricket 2020 60min\n")
                .append("10:45 am : Arduino Bonanza 30min\n")
                .append("11:15 am : Enigma Challenge 45min\n")
                .append("01:00 pm : Digital Tresure Hunt 60min\n")
                .append("02:00 pm : Monti Carlo or Bust 60min\n")
                .append("03:00 pm : New Zealand Haka 30min\n")
                .append("03:30 pm : Indiano Drizzle 45min\n")
                .append("04:15 pm : Time Tracker sprint\n").toString();

        Team team = new Team();
        team.addActivity(new Activity("Giant Digital Graffiti 45min"));
        team.addActivity(new Activity("Cricket 2020 60min"));
        team.addActivity(new Activity("Arduino Bonanza 30min"));
        team.addActivity(new Activity("Digital Tresure Hunt 60min"));
        team.addActivity(new Activity("Enigma Challenge 45min"));
        team.addActivity(new Activity("Monti Carlo or Bust 60min"));
        team.addActivity(new Activity("New Zealand Haka 30min"));
        team.addActivity(new Activity("Indiano Drizzle 45min"));
        team.addActivity(new Activity("Time Tracker sprint"));
        assertEquals(expected, team.toString());
    }

}
package com.deloitte.awaydayscheduler;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * <p>This class represents an Activity.</p>
 *
 * @author Taiwo O.
 */
public class Activity implements Comparable<Activity> {

    private final String description;

    public final int duration;

    private static final String DESCRIPTION_PATTERN = "(?i)^(.+ )+\\b((sprint$)|(\\d+min$))\\b";

    private static final String DURATION_PATTERN = "(?i)\\b((sprint$)|(\\d+min$))\\b";

    private static final Pattern durationPattern = Pattern.compile(DURATION_PATTERN);

    /**
     * <p>Checks if the given string matches {@link #DESCRIPTION_PATTERN}.</p>
     *
     * @param description the string to be examined
     * @return {@code true} if the given string matches {@link #DESCRIPTION_PATTERN}
     */
    public static boolean isActivity(String description) {
        return description.matches(DESCRIPTION_PATTERN);
    }

    /**
     * <p>Returns the duration sequence of the activity's description.</p>
     *
     * @return the duration sequence of the activity's description
     */
    public String getDurationString() {
        Matcher matcher = durationPattern.matcher(description);
        matcher.find();
        return matcher.group(1);
    }

    /**
     * <p>Computes the integer representation of the activity's duration.</p>
     *
     * @return the integer representation of the activity's duration
     */
    public int computeDuration() {
        int duration = 0;
        String durationString = getDurationString();
        Matcher digitMatcher = Pattern.compile("^(\\d+)").matcher(durationString);
        if (durationString.matches("(?i)^\\b(sprint)\\b$")) {
            duration = 15;
        } else if (digitMatcher.find()) {
            duration = Integer.parseInt(digitMatcher.group(1));
        }
        return duration;
    }

    /**
     * <p>Constructs an activity with the specified string.</p>.
     *
     * @param description a string that describes an activity
     * @throws IllegalArgumentException if {@link #isActivity(String description)} returns {@code false}
     */
    public Activity(String description) throws IllegalArgumentException {
        if (!isActivity(description)) {
            throw new IllegalArgumentException(
                    String.format("%s does not match regex %s", description, DESCRIPTION_PATTERN));
        }

        this.description = description;
        duration = computeDuration();
    }

    /**
     * <p>Constructs an activity with the specified description and duration.</p>.
     *
     * @param description  a string that describes the activity
     * @param duration the activity length
     */
    public Activity(String description, int duration){
        this.description = description;
        this.duration = duration;
    }

    /**
     * <p>Returns a string representation of an {@link Activity} object.</p>
     *
     * @return {@link #description}.
     */
    public String toString() {
        return description;
    }

    /**
     * <p>Compares this {@link Activity} object to the argument {@code anotherActivity} object.</p>
     * @param anotherActivity the {@link Activity} to be compared.
     * @return the signed mumerical comparison of this {@link Activity} object {@link #duration} to the argument {@code anotherActivity} object {@link #duration}
     */
    @Override
    public int compareTo(Activity anotherActivity) {
        return Integer.compare(this.duration, anotherActivity.duration);
    }
}
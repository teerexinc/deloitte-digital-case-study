package com.deloitte.awaydayscheduler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>This class represents a Parser for the application input.</p>
 *
 * @author Taiwo O.
 */
public class Parser {

    /**
     * <p>Parse {@code inputStream} to {@link List}<{@link Activity}>}.</p>
     *
     * @param inputStream an input stream
     * @return {@link List}<{@link Activity}>
     * @throws IOException if an I/O error occurs opening the file
     */
    public static List<Activity> parse(InputStream inputStream) throws IOException {
        List<Activity> activities = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
            String description;
            while ((description = br.readLine()) != null) {
                activities.add(new Activity(description));
            }
        }
        return activities;
    }

}

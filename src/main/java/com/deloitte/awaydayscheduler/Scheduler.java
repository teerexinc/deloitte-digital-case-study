package com.deloitte.awaydayscheduler;

import edu.princeton.cs.algs4.BST;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

/**
 * <p>This class is the Away Day Scheduler.</p>
 *
 * @author Taiwo O.
 */
public class Scheduler {

    public final static Logger LOGGER = Logger.getLogger(Scheduler.class.getName());

    /**
     * <p>Schedule {@code activities}.</p>
     *
     * @param activities the activities to schedule
     * @return list of teams
     */
    public static List<Team> schedule(List<Activity> activities) {
        List<Team> teams = new ArrayList();
        // binary search tree with key (maximum activity duration that can fit each team) and value (index of team in teams List).
        BST<Integer, Integer> teamDurationTree = new BST<>();
        activities.forEach(activity -> {
            int key = 0;
            try {
                // get the smallest team in the tree greater than or equal to activity.duration.
                key = teamDurationTree.ceiling(activity.duration);
            } catch (NoSuchElementException ex) {
                LOGGER.finest(String.format("No team can fit %s\n%s", activity, ex));
            } catch (NullPointerException ex) {
                LOGGER.finest(String.format("No team can fit %s\n%s", activity, ex));
            }
            int binId = 0;
            if (key == 0) {
                // no team is large enough, start a new team
                teams.add(new Team());
                binId = teams.size();
            } else {
                binId = teamDurationTree.get(key);
                // remove best team from the tree
                teamDurationTree.delete(key);
            }
            Team team = teams.get(binId - 1);
            // add activity to team and get the maximum activity duration
            int duration = team.addActivity(activity).getMaximumActivityDuration();

            // update team capacity and put team in tree unless team capacity is 0
            if (duration > 0) {
                teamDurationTree.put(duration, binId);
            }
        });

        teams.forEach(team -> {
            team.closeSchedule();
        });

        return teams;
    }

    /**
     * <p>Returns a string representation of {@link List}<{@link Team}>.</p>
     *
     * @param teams the {@link List}<{@link Team}>
     * @return the string representation of a {@link List}<{@link Team}>
     */
    public static String stringify(List<Team> teams) {
        int teamCounter = 1;
        StringBuilder stringBuilder = new StringBuilder();
        for (Team team : teams) {
            stringBuilder.append(String.format("Team %d:\n%s", (teamCounter++), team));
        }
        return stringBuilder.toString();
    }
}
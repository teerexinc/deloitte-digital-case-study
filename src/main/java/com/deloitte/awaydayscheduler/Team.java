package com.deloitte.awaydayscheduler;

import org.apache.commons.collections4.KeyValue;
import org.apache.commons.collections4.keyvalue.DefaultKeyValue;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>This class represents a Team</p>
 *
 * @author Taiwo O.
 */
public class Team {

    private static final int MORNING_BUCKET_SIZE = 180;

    private static final int AFTERNOON_BUCKET_SIZE = 240;

    private int morningBucket = MORNING_BUCKET_SIZE;

    private int afternoonBucket = AFTERNOON_BUCKET_SIZE;

    // 09:00 AM on an arbitrary date
    private static final Date NINE_AM = DateUtils.setMinutes(DateUtils.setHours(Date.from(Instant.now()), 9), 0);

    private List<KeyValue<Integer, Activity>> morningActivities = new ArrayList<>();

    private List<KeyValue<Integer, Activity>> afternoonActivities = new ArrayList<>();

    /**
     * <p>Return the maximum activity duration that can fit this {@link Team} object.</p>
     *
     * @return the maximum activity duration that can fit this {@link Team} object
     */
    public int getMaximumActivityDuration() {
        return Math.max(morningBucket, afternoonBucket);
    }

    /**
     * <p>Formats the date/time elapsed from {@link #NINE_AM} using {@code hh:mm a}.</p>
     *
     * @param minutes the minutes elapsed from NINE_AM
     * @return the formatted start time of an activity i.e. 9:00 am
     */
    public static String getStartTime(final int minutes) {
        return DateFormatUtils.format(DateUtils.addMinutes(NINE_AM, minutes), "hh:mm a").toLowerCase();
    }

    /**
     * <p>Adds an activity to the team and returns the maximum activity duration that can fi this {@link Team} object.</p>
     *
     * @param activity the activity to add to this {@link Team} object
     * @return this {@link Team} object
     * @throws IllegalArgumentException if the given activity cannot fit either of the buckets in this {@link Team} object
     */
    public Team addActivity(Activity activity) throws IllegalArgumentException {
        int activityDuration = activity.duration;
        if (activityDuration > morningBucket && activityDuration > afternoonBucket) {
            throw new IllegalArgumentException(String.format("The duration of '%s' does not fit team", activity));
        }
        if ((morningBucket <= afternoonBucket || activityDuration > afternoonBucket) && activityDuration <= morningBucket) {
            morningActivities.add(new DefaultKeyValue<>(MORNING_BUCKET_SIZE - morningBucket, activity));
            morningBucket -= activityDuration;
        } else if (activityDuration <= afternoonBucket) {
            afternoonActivities.add(new DefaultKeyValue<>(AFTERNOON_BUCKET_SIZE - afternoonBucket, activity));
            afternoonBucket -= activityDuration;
        }
        return this;
    }

    /**
     * <p>Appends the {@code lunchBreak} activity to {@link #morningActivities} and the {@code staffMotivation} activity to {@link #afternoonActivities}.
     * </p>
     */
    public void closeSchedule() {
        int lunchBreakDuration = morningBucket + 60;
        final Activity lunchBreak = new Activity(String.format("Lunch Break %dmin", lunchBreakDuration));

        morningActivities.add(new DefaultKeyValue<>(MORNING_BUCKET_SIZE - morningBucket, lunchBreak));
        morningBucket -= morningBucket;
        // staff motivation can start no earlier than 4:00 and no later than 5:00
        if (afternoonBucket <= AFTERNOON_BUCKET_SIZE - MORNING_BUCKET_SIZE) {
            final Activity staffMotivation = new Activity("Staff Motivation Presentation", 0);
            afternoonActivities.add(new DefaultKeyValue<>(AFTERNOON_BUCKET_SIZE - afternoonBucket, staffMotivation));
        }
    }

    /**
     * <p>Returns a string representation of a {@link Team} object.</p>
     *
     * @return a string representation of a {@link Team} object.
     */
    public String toString() {
        StringBuilder activities = new StringBuilder();
        morningActivities.forEach(activity -> activities.append(String.format("%s : %s\n", getStartTime(activity.getKey()), activity.getValue())));
        afternoonActivities.forEach(activity -> activities.append(String.format("%s : %s\n", getStartTime(AFTERNOON_BUCKET_SIZE + activity.getKey()), activity.getValue())));
        return activities.toString();
    }

}
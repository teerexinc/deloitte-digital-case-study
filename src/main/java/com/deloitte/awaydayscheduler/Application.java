package com.deloitte.awaydayscheduler;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * <p>This is the main application.</p>
 *
 * @author Taiwo O.
 */
public class Application {

    public final static Logger LOGGER = Logger.getLogger(Application.class.getName());

    /**
     * <p>Main application.</p>
     *
     * @param args
     * @throws IOException if there is a problem loading the input file name
     */
    public static void main(String[] args) throws IOException {

        String inputFileName = System.getProperty("input.filename");

        LOGGER.info(String.format("Starting Away Day Scheduler using input file: %s", inputFileName));

        try (InputStream activitiesInputStream =
                     ClassLoader.getSystemClassLoader().getResourceAsStream(inputFileName)) {

            List<Activity> activities = Parser.parse(activitiesInputStream);
            List<Team> schedule = Scheduler.schedule(activities);
            LOGGER.info(String.format("\n-----Output-----\n%s\n-----------------", Scheduler.stringify(schedule)));
        }
    }

}

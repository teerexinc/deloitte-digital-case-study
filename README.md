# AwayDayScheduler

Solution for Technical Test - Java Back End Case Study (Deloitte Digital).
[Java_Back_End_Case_Study.pdf](https://trello-attachments.s3.amazonaws.com/5b591c4eade040b946786914/5b591e27873fea54269e4b88/136c9a5f6d6341a9cada9f513ff9e36a/Java_Back_End_Case_Study.pdf).

## Assumptions

* Each line on the Input file e.g. activities.txt conforms to the regex format /^(.+ )+\b((sprint$)|(\d+min$))\b/g
* The input i.e activities.txt can be loaded in application memory
* There's no specification for the duration of the lunch break after lunch is served
* An activity can be performed by one and only one team except lunch break and staff motivation
* The solution can contain an arbitrary number of teams
* No abstraction for employee which implies that a team can consists of any arbitarty number of employee
* The duration of each activity is not more than 240 minutes (between 1pm - 5pm)
* Lunch break is an activity with variable length at least 60 mins
* If there is not enough activities to fill a team's schedule up to 4pm, staff motivation will not appear on the schedule

## Solution overview (psedocode)

[Application](./src/main/java/com/deloitte/awaydayscheduler/Application.java)

```java
Application
  |_ parseActivities(resourceIdentifier: String): List<Activity> // resource identifier should follow the format [scheme:][//authority][path] e.g. http://example.com/activities.txt, file://~/activities.txt
```

[Scheduler](./src/main/java/com/deloitte/awaydayscheduler/Scheduler.java)

```java
Scheduler
  |_ schedule(Activities: List<Activity>): List<Team> {
      var teams = new List<Team>()
      var minutesTree = new BinarySearchTree<int, int>() // Binary Search Tree with Key (maximum activity duration that can fit each team) and Value (index of team in teams List).
      activities.forEach(activity -> {
        var key = minutesTree.ceiling(activity.minutes) // get the smallest team in minutesTree greater than or equal to activity.minutes
        var binId = minutesTree.get(key)
        if (!binId) {
          teams.add(new Team())
          binId = team.length
        } else {
          minutesTree.remove(key)
        }
        var team = teams.get(binId - 1)
        var minutes = team.addActivity(activity);
        if (minutes > 0) {
          minutesTree.put(minutes, binId)
        }
      }
      return teams;
    }
```

[Activity](./src/main/java/com/deloitte/awaydayscheduler/Activity.java)

```java
Activity
  |_ name: String
  |_ length: int
```

[Team](./src/main/java/com/deloitte/awaydayscheduler/Team.java)

```java
Team
  |_ List<keyValue<int, Activity>> // key is the start time of activity in integer
  |_ int morningBucketSize = 180
  |_ int afternoonBucketSize = 240
  |_ lunchBreak: Activity // initialized on constructor
  |_ staffMotivation: Activity // initialized on constructor
  |_ addActivity(activity: int): int { // return the maximun activity duration that can fit the team
      if ((morningBucket <= afternoonBucket || activityDuration > afternoonBucket) && activity.length <= morningBucket) {
        // add activity to morning bucket
        morningBucket -= activity.length
      } else {
        // add activity to afternoon bucket
        afternoonBucket -= activity.length
      }
      return Max(morningBucket, afterNoonBucket)
    }
  |_ closeSchedule() {
    // adjust the afternoon bucket size
    afternoonBucket += morningBucket;
    // add lunch break to the morning bucket
    morningBucket -= lunchBreak.length
    if (afternoonBucket <= 180) {
      // add staff motivation to afternoon bucket
      afternoonBucket -= staffMotivation.length
    }
  }
```

This psedocode above uses the Best fit (decreasing) bin packing algorithm to find the best team (bin) that fit's an activity.

There are two buckets within each team - morning and afternoon. The algorithm keeps track of the maximum activity that can fit each team i.e. Max(morning, afternoon). For a given activity, if a team has a perfect fit bucket but the bucket is the minimum, the algorithm could allocate the activity to some other team that seems the best fit at the team level. Hence, creating a temporary starvation of such buckets.

## Running the application

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

* Java (>=1.8 required)
* Clone the project

```sh
git clone https://teerexinc@bitbucket.org/teerexinc/deloitte-digital-case-study.git && cd deloitte-digital-case-study
```

### Run application

Run main application with default input 'activities.txt'

```sh
./gradlew run
```

Run main application with a supplied input. The file should be within ./src/main/resources

```sh
./gradlew run -D input.filename=anotherActivities.txt
```

### Running the tests

You can view the test report by opening the HTML output file, located at ./build/reports/tests/test/index.html.

```sh
./gradlew test
```

## Built with

* Binary Search Tree - [edu.princeton.cs.algs4.BST<Key,Value>](https://algs4.cs.princeton.edu/code/javadoc/edu/princeton/cs/algs4/BST.html)
* Gradle 4.8
* JUnit 5